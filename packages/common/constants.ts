export enum UserStatus {
  TEMP = "temporary",
  UNVERIFIED = "unverified",
  INCOMPLETE = "incomplete",
  ACTIVE = "active",
  CANCELLED = "cancelled",
  SUSPENDED = "suspended",
}

export enum AuthProviders {
  EMAIL = "email",
  GOOGLE = "google",
  MICROSOFT = "microsoft",
}

export enum AccountType {
  INDIVIDUAL = "individual",
  ORGANIZATION = "organization",
}

export enum UserRoles {
  ADMIN = "admin",
  MEMBER = "member",
  VIEWER = "viewer",
  EDITOR = "editor",
  SUPPORT = "support",
  DEVELOPER = "developer",
}

export enum TokenTypes {
  REFRESH_TOKEN = "refreshToken",
  VERIFY_TOKEN = "verifyToken",
  ACCESS_TOKEN = "accessToken",
}

export enum EmailTemplate {
  AUTH_CREATE_USER = "AUTH_CREATE_USER",
  AUTH_RESET_PASSWORD = "AUTH_RESET_PASSWORD",
  PRINT_JOB_CONFIRMATION = "PRINT_JOB_CONFIRMATION",
  ORDER_INVOICE = "ORDER_INVOICE",
}

export enum NotificationCategories {
  PROPERTY_FOLLOW_UP = "propertyFollowUp",
}

export enum ProductCategories {
  PRINT = "print",
}
export enum OrderStatus {
  paid = "PAID",
  pending = "PENDING",
}
export enum Units {
  IMPERIAL = "imperial",
  METRIC = "metric",
}
