import React, { useState } from "react";

type ContextProps = {
  state: boolean;
  setState: React.Dispatch<React.SetStateAction<boolean>>;
};

const noop = () => null;
const FeatureTogglesContext = React.createContext<Partial<ContextProps>>({});
const { Consumer, Provider } = FeatureTogglesContext;

export const ToggleWrapper: React.FC<{}> = ({ children }) => {
  const [state, setState] = useState(false);
  return <Provider value={{ state, setState }}>{children}</Provider>;
};

export const Toggle: React.FC<{}> = ({ children, ...props }) => (
  <Consumer>
    {({ state: show, setState }) => {
      const onClick = () => {
        setState && setState(!show);
      };
      return (
        <button {...props} onClick={onClick}>
          {children}
        </button>
      );
    }}
  </Consumer>
);
export const ToggleContent: React.FC<{}> = ({ children, ...props }) => (
  <Consumer>
    {({ state: show, setState }) => {
      return (show && children) || noop;
    }}
  </Consumer>
);

/* Usage

    <ToggleWrapper>
        <Toggle>Toggle Trigger</Toggle>
        <ToggleContent>Content to toggle</ToggleContent>
    </ToggleWrapper>

*/
