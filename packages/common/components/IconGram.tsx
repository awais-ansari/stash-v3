export const IconGram = ({ src }: IconGramProps) => (
  <img
    className="w-4 mx-auto"
    src={`https://icongr.am/${src}.svg?size=18&color=currentColor`}
    alt=""
  />
);

interface IconGramProps {
  src: string;
}
